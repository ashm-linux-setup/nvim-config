#!/usr/bin/env bash

script_dir=$(realpath $(dirname "${BASH_SOURCE}"))


# install nvim configuration
link_target_directory="${script_dir}/nvim"
rm -rf "$HOME/.config/nvim"
ln -s "${script_dir}/nvim" "${HOME}/.config/nvim"


# install man page
man_file_name="ashm_nvim.1"
gzip -c "${script_dir}/${man_file_name}" > "${script_dir}/${man_file_name}.gz"
mv -f "${script_dir}/${man_file_name}.gz" "${ASHM_LOCAL_MANPAGE_HOME}/man1"

