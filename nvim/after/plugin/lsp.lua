
local on_attach = function(_,bufnr)
    local opts = {buffer = bufnr, remap = false}

    vim.keymap.set("n", "gd"             , function() vim.lsp.buf.definition() end, opts)
    vim.keymap.set("n", "K"              , function() vim.lsp.buf.hover() end, opts)
    vim.keymap.set("n", "<leader>vws"    , function() vim.lsp.buf.workspace_symbol() end, opts)
    vim.keymap.set("n", "<leader>vd"     , function() vim.diagnostic.open_float() end, opts)
    vim.keymap.set("n", "[d"             , function() vim.diagnostic.goto_next() end, opts)
    vim.keymap.set("n", "]d"             , function() vim.diagnostic.goto_prev() end, opts)
    vim.keymap.set("n", "<leader>vca"    , function() vim.lsp.buf.code_action() end, opts)
    vim.keymap.set("n", "<leader>vrr"    , function() vim.lsp.buf.references() end, opts)
    vim.keymap.set("n", "<leader>vrn"    , function() vim.lsp.buf.rename() end, opts)
    vim.keymap.set("i", "<lader>h"          , function() vim.lsp.buf.signature_help() end, opts)
end


local mason = require('mason')
mason.setup()


local mason_lspconfig = require('mason-lspconfig')
mason_lspconfig.setup({
    ensure_installed = {
        'rust_analyzer',
        'pyright',
        'lua_ls',
        'gopls',
        'bashls',
        'tailwindcss',
        'ts_ls',
    }
})



local lspconfig = require('lspconfig')
local capabilities = require('cmp_nvim_lsp').default_capabilities()

lspconfig.rust_analyzer.setup {
    on_attach = on_attach,
    capabilities = capabilities,
}

lspconfig.pyright.setup {
    on_attach = on_attach,
    capabilities = capabilities,
}

lspconfig.lua_ls.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        Lua = {
            diagnostics = {
                globals = {'vim'},
            },
        },
    },
}

lspconfig.gopls.setup {
    on_attach = on_attach,
    capabilities = capabilities,
}


lspconfig.ts_ls.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    filetypes = { 'javascript', 'javascriptreact', 'javascript.jsx', 'typescript', 'typescriptreact', 'typescript.tsx' },
}

