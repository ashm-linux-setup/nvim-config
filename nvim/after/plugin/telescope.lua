local b = require('telescope.builtin')

vim.keymap.set('n', '<leader>ff', b.find_files, {})
vim.keymap.set('n', '<leader>fg', b.live_grep, {})
vim.keymap.set('n', '<leader>fb', b.buffers, {})
vim.keymap.set('n', '<leader>fgb', b.git_branches, {})
vim.keymap.set('n', '<leader>fgc', b.git_commits, {})
vim.keymap.set('n', '<leader>fgs', b.git_status, {})
vim.keymap.set('n', '<leader>fgh', b.git_stash, {})

