local dap, dapui = require('dap'), require('dapui')
local codelldb = require('mason-registry').get_package('codelldb'):get_install_path() .. '/codelldb'

dap.adapters.codelldb = {
  type = 'server',
  port = "${port}",
  executable = {
    command = codelldb,
    args = {"--port", "${port}"},
  }
}

dap.configurations.rust = {
    {
        name = 'Debug with codelldb',
        type = 'codelldb',
        request = 'launch',
        program = function()

            local dir_path = vim.loop.cwd()
            local dir_name = vim.fn.fnamemodify(dir_path, ':t')
            local target_file =  dir_path .. "/target/debug/" .. dir_name

            local f=io.open(target_file,"r")
            if f~=nill then

                io.close()
                return target_file
            else
                return vim.fn.input({
                    prompt     = 'Path to executable: ',
                    default    = vim.fn.getcwd() .. '/',
                    colpletion = 'file'
                })
            end
        end,
--        program = vim.fn.getcwd() .. '/target/debug/$CWD',
--        program = function()
--            return vim.fn.input({
--                prompt = 'Path to executable: ',
--                default = vim.fn.getcwd() .. '/',
--                completion = 'file',
--            })
--        end,
        cwd = '${workspaceFolder}',
        stopOnEntry = false,
    },
}

vim.keymap.set('n', '<F5>',dap.continue)
vim.keymap.set('n', '<F1>',dap.step_over)
vim.keymap.set('n', '<F2>',dap.step_into)
vim.keymap.set('n', '<F2>',dap.step_out)
vim.keymap.set('n', '<leader>b',dap.toggle_breakpoint)


dapui.setup()

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open()
end

dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close()
end

dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close()
end
