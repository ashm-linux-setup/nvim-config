local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then

	vim.fn.system({
		'git',
		'clone',
		'--filter=blob:none',
		'https://github.com/folke/lazy.nvim.git',
		'--branch=stable',
		lazypath,
	})
end

vim.opt.rtp:prepend(lazypath)

require('lazy').setup({

    -- colour schemes
    { 'ellisonleao/gruvbox.nvim' },
    { 'Tsuzat/NeoSolarized.nvim',
        lazy = false, -- make sure we load this during startup if it is your main colorscheme
        priority = 1000, -- make sure to load this before all the other start plugins
    },
    { 'neanias/everforest-nvim',
       version = false,
       lazy = false,
       priority = 1000,
    },
    { 'nvim-treesitter/nvim-treesitter',
       build = ':TSUpdate',
       branch = "master",
    },

    -- navigation
    { 'nvim-lua/plenary.nvim' },
    { 'nvim-telescope/telescope.nvim',
      tag = '0.1.4',
      dependencies = {
          'nvim-lua/plenary.nvim'
      }
    },

    -- lsp
    {
        { 'williamboman/mason.nvim' },
        { 'williamboman/mason-lspconfig.nvim' },
        { 'neovim/nvim-lspconfig' },
    },

    -- autocompletions
    {'hrsh7th/nvim-cmp'},

    -- completion sources
    {'hrsh7th/cmp-nvim-lsp'},

    -- snippets engine
    {
        'L3MON4D3/LuaSnip',
        tag = "v2.*", -- follow latest release
        run = "make install_jsregexp"
    },
    {'saadparwaiz1/cmp_luasnip'},

    -- snippets sources
    {'rafamadriz/friendly-snippets'},

    -- dap
    { 'mfussenegger/nvim-dap'},
    { "rcarriga/nvim-dap-ui", dependencies = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"} },
    { 'rcarriga/nvim-dap-ui',
      dependencies = {
          'mfussenegger/nvim-dap'
      }
    },

    -- ui

    -- editing
    { 'kylechui/nvim-surround',
      version = '*',
    },

    -- terminal
    { 'voldikss/vim-floaterm' },
})
