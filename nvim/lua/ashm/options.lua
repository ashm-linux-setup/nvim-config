-- leader
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- clipboard
vim.o.clipboard = 'unnamedplus'

-- line numbers
vim.o.number = true
vim.o.relativenumber = false

-- tabs & indentation
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = true

-- line wrapping
vim.o.wrap = false

-- search settings
vim.o.hlsearch = false
vim.o.ignorecase = true
vim.o.smartcase = true

-- cursor line
vim.o.cursorline = true

-- appearance
vim.o.termguicolors = true
vim.o.background = "dark"
vim.o.signcolumn = "yes"

-- backspace
vim.o.backspace = "indent,eol,start"

-- listchars
vim.o.listchars = "eol:¶,trail:●,space:·,tab:»·,nbsp:␣,extends:…"

